﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileUserInput
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a whole number below 10.");
            var i = int.Parse(Console.ReadLine());
            var counter = 20;
            do
            {
                var a = i + 1;
                Console.WriteLine($"This is line {a}");
                i++;
            }
            while (i < counter);
        }
    }
}
